
import fs from 'fs'
import path from 'path'

const readdir_recursive = (dir_path) => {
  const file_tree = [];
  const cfiles = fs.readdirSync(dir_path);
  for (const cfile of cfiles) {
    const tree_entry = {
      type: "file",
      name: cfile
    }

    const cfpath = path.resolve(dir_path, cfile);
    if (fs.lstatSync(cfpath).isDirectory()) {
      tree_entry.type = "directory";
      tree_entry.tree = readdir_recursive(cfpath);
    }

    file_tree.push(tree_entry);
  }

  return file_tree;
}

export default class Context {
  constructor(full_path) {
    this.full_path = full_path;

    this.file_tree = readdir_recursive(full_path);


    for (const file_entry of this.file_tree) {
      if (file_entry.name == "src" && file_entry.type == "directory") {
        for (const sfile of file_entry.tree) {
          if (sfile.name == "index.js" || sfile.name == "index.mjs") {
            this.src_index_js = path.resolve(this.full_path, "src", sfile.name);
          }
        }
      }

      if (file_entry.name == "theme.less" && file_entry.type == "file") {
        this.theme_less = path.resolve(this.full_path, "theme.less");
      }
    }

  }
}
