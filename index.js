

import fs from 'fs'
import path from 'path'

import Context from './context.js'

export default class FlyProject {
  static async construct() {
    try {
      let _this = new FlyProject();
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(app_path) {
    console.log("FLY PROJECT");
    const mellisuga_json_path = path.resolve(app_path, "mellisuga.json");
    if (!fs.existsSync(mellisuga_json_path)) console.error(new Error(`mellisuga.json does not exist in ${app_path}`));

    this.pages = {
      list: []
    };
    const pages_path = path.resolve(app_path, "pages");
    if (fs.existsSync(pages_path) && fs.lstatSync(pages_path).isDirectory()) {
      const pages_config_path = path.resolve(pages_path, "config.json");
      if (fs.existsSync(pages_config_path)) {
        try {
          this.pages.config = JSON.parse(fs.readFileSync(pages_config_path, 'utf8'));
        } catch (e) {
          console.log("ERROR READING PAGES CONFIG!");
          console.error(e.stack);
        }
      }

      const dir_files = fs.readdirSync(pages_path);
      for (const dfile of dir_files) {
        const dfile_path = path.resolve(pages_path, dfile);
        if (fs.lstatSync(dfile_path).isDirectory()) {
          this.pages.list.push(new Context(dfile_path));
        }
      }

    }

    const globals_path = path.resolve(app_path, "globals");
    this.globals = new Context(globals_path);
  }


}
